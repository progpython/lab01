# -*- coding: UTF-8 -*-
'''
Python version: 3.4
Created on 2014-09-19

@author: Johnny Tsheke -- UQAM 
Calcul des taxes: TPS (5%) et TVQ (9.975%)
'''
montantSansTaxes = input("Entrer le montant sans taxes svp\n")
montantSansTaxes=eval(montantSansTaxes)
montantTPS = montantSansTaxes * 0.05
montantTVQ = montantSansTaxes * 0.09975
montantAvecTaxes = montantSansTaxes + montantTPS + montantTVQ
print("Le montant sans taxes = ", montantSansTaxes)
print("Le montant de la TPS = ", montantTPS)
print("Le montant de la TVQ = ", montantTVQ)
print("Le montant total avec taxes = ",montantAvecTaxes)